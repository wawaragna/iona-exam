import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { createStore } from 'redux';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Provider } from 'react-redux'

import Home from './pages/Home';
import CatDetail from './pages/CatDetail';

import reducers from './redux/reducers';
const store = createStore( reducers );

/**
 * Initial to run the application
 */
function App() {
  return (
    <div>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/:catID" component={CatDetail}></Route>
            <Route path="/" component={Home}></Route>
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
