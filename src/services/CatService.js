import axios from 'axios';

/**
 * Service class to handle cat requests
 */
class CatService {

    /**
     * Set the apiURL
     */
    static apiURL = "https://api.thecatapi.com/v1";

    /**
     * Set the common headers for the request
     * 
     * # Temporarily blank but just setup for possible 
     * # auth header later
     */
    static headers = {};

    /**
     * Retrieves the breed list
     */
    static getBreedList(){
        let url = this.apiURL + "/breeds";
        return axios.get(url, { headers: this.headers })
    }

    /**
     * Searches the cat images
     * @param {Object} params Contains the information 
     *                        for filtering the cat search 
     */
    static getByBreed(params){
        let url = this.apiURL + "/images/search";
        return axios.get(url, { params: params, headers: this.headers })
    }

    /**
     * Retrieve the cat information
     * @param catID Reference id for the specific cat
     */
    static getCatInfo(catID){
        let url = this.apiURL + "/images/" + catID;
        return axios.get(url, { headers: this.headers })
    }

}


export default CatService