import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { 
    setBreedList, 
    setSelectedBreed,
    setCatList,
    setPage,
    setLoading,
    setHideLoadMore
} from "../redux/actions/CatActions";

import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CatService from '../services/CatService';

/**
 * Component that is set for the home route
 */
class Home extends Component {
    /**
     * Handles the select event from the breed selection
     * 
     * Resets the initial state values and update the 
     * catList based on the selected breed
     */
    changeSelectedBreed = ( evt ) => {
        let val = evt.target.value;

        this.props.setPage(1);
        this.props.setHideLoadMore(false);
        this.props.setCatList( [] );

        this.updateSelectedBreed( val, 1 );
    }

    /**
     * Convenience function to update the selected
     * breed in state.
     * 
     * @param id Reference id for the target cat breed
     * @param page Page number to be loaded
     */
    updateSelectedBreed = ( id, page ) => {
        if( id ){
            this.getCatList( id, page );
        }
        this.props.setSelectedBreed( id );
    }

    /**
     * Logic for retrieving cats information.
     * 
     * Since the API's page has issues with page parameter,
     * limit will be identified via current "page" to be loaded.
     * 
     * Order is also set to `asc` to have consistency when 
     * retrieving the data.
     * 
     * @param breedId Reference id for the target cat breed
     * @param page Page number to be loaded
     */
    getCatList = ( breedId, page ) => {
        let limit = page * 10;
        
        let data = {
            breed_id: breedId,
            limit: limit,
            order: "asc"
        }

        let catListPromise = CatService.getByBreed( data );

        this.props.setLoading(true);

        catListPromise.then( result => {
            let catList = result.data;

            if( catList.length < limit ){
                this.props.setHideLoadMore(true);
            } else {
                this.props.setPage( this.props.cat.page + 1 );
            }

            this.props.setCatList( catList );

            this.props.setLoading(false);
        });
    }


    /**
     * Handles the load more button
     */
    loadMoreCats = () => {
        this.getCatList(this.props.cat.selectedBreed, this.props.cat.page);
    }

    /**
     * Handles retrieving the breed list information
     */
    retrieveBreedList = () => {
        let breedPromise = CatService.getBreedList();

        breedPromise.then( result => {
            let breedList = result.data;
            this.props.setBreedList( breedList );
        } )
    }

    /**
     * Handles component mounting.
     * 
     * Searches for @breed on the url parameters
     * and use it as a basis of retrieving the 
     * cat list.
     */
    componentDidMount(){
        let query = new URLSearchParams(this.props.location.search);
        let breed = query.get("breed", undefined);

        this.retrieveBreedList();

        if( breed ){
            this.updateSelectedBreed(breed, this.props.cat.page);
        }
    }

    /**
     * Renders the component
     */
    render(){
        return (
            <Container style={{padding: "20px 0"}}>
                <h1>Cat Browser</h1>

                <Row style={{padding: "10px 0"}}>
                    <Col xs={12} sm={6} md={3} >
                        <Form  >
                            <Form.Group>
                                <Form.Label>
                                    Breed
                                </Form.Label>
                                <Form.Control as="select" onChange={this.changeSelectedBreed} value={this.props.cat.selectedBreed}>
                                    <option value="">Select a breed</option>
                                    {
                                        Array.isArray(this.props.cat.breedList) &&
                                        this.props.cat.breedList.map( (breedInfo, index) => {
                                            return (
                                                <option value={breedInfo.id} key={breedInfo.id}> { breedInfo.name } </option>
                                            )
                                        } )
                                    }
                                </Form.Control>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>

                <Row>
                    {
                        this.props.cat.catList.length ? (
                            this.props.cat.catList.map( catInfo => (
                                <Col key={catInfo.id} xs={12} sm={6} md={3}>
                                    <Card style={{marginBottom: 20}}>
                                        <Card.Img variant="top" src={catInfo.url} />
                                        <Card.Body>
                                            <Button block variant="primary" className="text-center" href={"/" + catInfo.id}>View details</Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            ) )
                        ) : (
                            <Col xs={12}>
                               <p>No cats available</p>
                            </Col>
                        )
                        
                    }
                </Row>

                <Row style={{marginTop: 10}}>
                    <Col xs={12} sm={6} md={3} >
                        { this.props.cat.hideLoadMore === false && (
                            <Button variant="success" onClick={this.loadMoreCats} 
                                    disabled={((this.props.cat.selectedBreed.length === 0) || (this.props.cat.isLoading === true) )}>
                                { this.props.cat.isLoading ? "Loading cats..." : "Load More"}
                            </Button>
                        )}
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const { cat } = state
    return { cat } 
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        setBreedList,
        setSelectedBreed,
        setCatList,
        setPage,
        setLoading,
        setHideLoadMore
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Home);