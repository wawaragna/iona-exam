import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';

import CatService from '../services/CatService';

import { setCatInfo } from "../redux/actions/CatActions";

/**
 * Component that is set for the cat detail
 */
class CatDetail extends Component {

    /**
     * Retrieves the cat information based on the 
     * set catID
     * 
     * @param catId Specific id for the cat image
     */
    getCatInformation = ( catId ) => {
        let catInfoPromise = CatService.getCatInfo( catId );

        catInfoPromise.then( result => {
            let catInfo = result.data;

            if( catInfo ){
                catInfo = {
                    ...catInfo.breeds[0],
                    photo: catInfo.url
                }
            }

            this.props.setCatInfo(catInfo);
        });
    }

    /**
     * Handles component mounting.
     * 
     * Retrieves the catID from the URI then
     * retrieves the information of the cat.
     */
    componentDidMount(){
        let { catID } = this.props.match.params;
        this.getCatInformation(catID);
    }

    /**
     * Renders the component
     */
    render(){
        return (
            <Container style={{ paddingTop: 20, paddingBottom: 20}}>
                    { 
                        this.props.cat.info ? (
                            <Card>
                                <Card.Header>
                                    <Button href={ "/?breed=" + this.props.cat.info.id }>Back</Button>
                                </Card.Header>
                                <Card.Img src={this.props.cat.info.photo}></Card.Img>
                                <Card.Body>
                                    <h4>{ this.props.cat.info.name }</h4>
                                    <h5>{ "Origin: " + this.props.cat.info.origin}</h5>
                                    <h6> { this.props.cat.info.temperament } </h6>
                                    <p> { this.props.cat.info.description } </p>
                                </Card.Body>
                            </Card>
                        ) : (
                            <h5>Loading ... </h5>
                        )
                    }
            </Container>
        );
    }
}


const mapStateToProps = (state) => {
    const { cat } = state
    return { cat } 
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        setCatInfo
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(CatDetail);