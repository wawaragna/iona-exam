const setBreedList = breedList => {
    return {
        type: 'SET_BREED_LIST',
        payload: breedList,
    }
}

const setSelectedBreed = breedInfo => {
    return {
        type: 'SET_SELECTED_BREED',
        payload: breedInfo,
    }
}

const setCatList = catList => {
    return {
        type: 'SET_CAT_LIST',
        payload: catList,
    }
}

const setPage = (page) => {
    return {
        type: 'SET_PAGE',
        payload: page
    }
}

const setCatInfo = (catInfo) => {
    return {
        type: 'SET_CAT_INFO',
        payload: catInfo
    }
}

const setLoading = (setLoading) => {
    return {
        type: 'SET_LOADING',
        payload: setLoading
    }
}

const setHideLoadMore = (hideLoadMore) => {
    return {
        type: 'SET_HIDE_LOAD_MORE',
        payload: hideLoadMore
    }
}


export { setBreedList, setSelectedBreed, setCatList, setPage, setCatInfo, setLoading, setHideLoadMore }