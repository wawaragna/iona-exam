const INITIAL_STATE = {
    breedList: [],
    selectedBreed: "",
    catList: [],
    page: 1,
    info: null,
    isLoading: false,
    hideLoadMore: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "SET_BREED_LIST":
          var { breedList } = state;
          breedList = action.payload;
          return Object.assign( {}, state, {breedList} );

        case "SET_SELECTED_BREED":
          var { selectedBreed } = state;
          selectedBreed = action.payload;
          return Object.assign( {}, state, {selectedBreed} );

        case "SET_CAT_LIST":
          var { catList } = state;
          catList = action.payload;
          return Object.assign( {}, state, {catList} );

        case "SET_PAGE":
          var { page } = state;
          page = action.payload;
          return Object.assign( {}, state, {page} );

        case "SET_CAT_INFO":
          var { info } = state;
          info = action.payload
          return Object.assign( {}, state, {info} );

        case "SET_LOADING":
          var { isLoading } = state;
          isLoading = action.payload;
          return Object.assign( {}, state, {isLoading} );

        case "SET_HIDE_LOAD_MORE":
          var { hideLoadMore } = state;
          hideLoadMore = action.payload;
          return Object.assign( {}, state, {hideLoadMore} );

      default:
        return state
    }
};