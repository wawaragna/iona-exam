import { combineReducers } from 'redux';

import CatReducer from './CatReducer';


/**
 * Combine reducers to be connected to the store
 * 
 * # Currently setup for future possible integrations
 */
export default combineReducers({ 
    cat: CatReducer,
});